package facci.richardgarcia.practicasqlite;

public class DBManager {
    //Nombre de la tabla
    public static final String TABLE_NAME = "articulos";
    //Campos
    public static final String ID = "codigo";
    public static final String NAME = "nombre";
    public static final String PRICE = "precio";

    //Sentencia SQL para crear la tabla
    //Se crea una tabla con el campo _id que sera un entero primary key autoincremental
    //nombre sera texto no null y telefono sera texto
    public static final String CREATE_TABLE = "create table " +TABLE_NAME+ " ("
            + ID + " integer primary key,"
            + NAME + " text not null,"
            + PRICE + " real);";
    //Luego vamos al metodo onCreate de DBHelper

    //Para cuando se elimine la tabla
    //public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
}

