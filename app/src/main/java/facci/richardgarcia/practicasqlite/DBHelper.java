package facci.richardgarcia.practicasqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
//Esta clase nos servira para abrir o crear la base de datos
//Tambien ayuda a actualizar la BD
public class DBHelper extends SQLiteOpenHelper{
    //Necesitamos 2 paremetros
    //Nombre del archivo de la base de datos ya que SQLite es un archivo
    private  static final String DB_NAME = "articulos.sqlite";
    //Versión de el esquema de la BD
    private static final int DB_SCHEME_VERSION = 1;
    //Opcion que tiene 4 parametros
    public DBHelper(Context context) {
        //Constructor del constructor padre, nos ayuda a la creacion y modificacion
        //SQLiteDatabase.CursorFactory factory se utiliza para implementar
        // validaciones extra sobre la base de datos
        super(context, DB_NAME, null, DB_SCHEME_VERSION);
    }
    //Se ejecuta cuando se crea la base de datos mediante sentencia SQL
    @Override
    public void onCreate(SQLiteDatabase db) {
        //Se crea una instancia de la clase
        db.execSQL(DBManager.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //Metodo para cuando se realize un cambio en la base de datos
       /* db.execSQL(DBManager.DELETE_TABLE);
        onCreate(db);*/

    }

}
