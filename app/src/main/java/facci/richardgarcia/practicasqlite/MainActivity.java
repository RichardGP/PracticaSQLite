package facci.richardgarcia.practicasqlite;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    //Se crea instancia de los editText
    EditText editTextCodigo, editTextNombre, editTextPrecio;
    Button buttonIngresar, buttonBuscar, buttonModificar, buttonEliminar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editTextCodigo = (EditText) findViewById(R.id.editTextCodigo);
        editTextNombre = (EditText) findViewById(R.id.editTextNombre);
        editTextPrecio = (EditText) findViewById(R.id.editTextPrecio);

        buttonIngresar = (Button) findViewById(R.id.buttonIngresar);
        buttonEliminar = (Button) findViewById(R.id.buttonEliminar);
        buttonBuscar = (Button) findViewById(R.id.buttonBuscar);
        buttonModificar = (Button) findViewById(R.id.buttonModificar);
        //Se asignar a los botones los metodos
        buttonIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Registrar(v);
            }
        });
        buttonBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Buscar(v);
            }
        });
        buttonModificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Modificar(v);
            }
        });
        buttonEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Eliminar(v);
            }
        });

    }
    //Metodo para registar los producto
    public void Registrar(View view){

        //Se crea una objeto de la clase con las operaciones de la DB (DBHelper)
        DBHelper admin = new DBHelper(this);
        //Abrir la base de dato en modo escritura y lectura
        //Hasta que no se hace la llamada la DB no se crea
        //getWritableDatabase crea la base de datos si no existe y la devuelve en
        //modo escritura y si existe solamente la devuelve
        SQLiteDatabase db = admin.getWritableDatabase();
        //Se crear las variables para recuperar el contendido de los editText
        String codigo = editTextCodigo.getText().toString();
        String nombre = editTextNombre.getText().toString();
        String precio = editTextPrecio.getText().toString();

        //Se realizan las validaciones para comprobar que el usuario a añadido valores
        if (!codigo.isEmpty() && !nombre.isEmpty() && !precio.isEmpty()){
            //Cuando se cumpla la condicion de que deben contener texto
            //Se crea un objeto de la clase ContentValues que se utilza para almacenar un conjunto de datos
            ContentValues registro = new ContentValues();
            //Se colocan los datos para almacenarlo dentro de la base de datos
            registro.put("codigo", codigo);
            registro.put("nombre", nombre);
            registro.put("precio", precio);

            //Se insertan los valores dentro de la tabla
            //Se para el nombre de la tabla, el segundo parametro en null, y el objeto que contiene los regisros
            db.insert(DBManager.TABLE_NAME, null, registro);

            //Se cierra la base de datos despues de utilizarla
            db.close();
            //Se limpiar los editText
            editTextCodigo.setText("");
            editTextNombre.setText("");
            editTextPrecio.setText("");
            //Mensaje para decirle al usuario que el registro fue exitoso
            Toast.makeText(this, "Ingreso exitoso", Toast.LENGTH_LONG).show();

        }else{
            //Se envia el toast con el mensaje
            Toast.makeText(this, "Debe llenar todos los campos", Toast.LENGTH_LONG).show();
        }


    }

    //Metodo para consultar un articulo o producto
    public void Buscar(View view){
        //Se crea una objeto de la clase con las operaciones de la DB (DBHelper)
        DBHelper admin = new DBHelper(this);
        //Abrir la base de dato en modo escritura y lectura
        SQLiteDatabase db = admin.getWritableDatabase();
        //Segun lo que el usuario ingrese el codigo se busca el producto
        String codigo = editTextCodigo.getText().toString();
        //Validacion para que el usuario no debe el campo vacio
        if (!codigo.isEmpty()){
            //Se crea un objeto de la clase cursor
            //La clase cursor nos va ayudar cuando queramos buscar el producto por su codigo
            //Metodo rawQuery ayuda a realizar la consulta
            //Entre comillas va el query que es la sentencia que queremos ejecutar para localizar los datos
            Cursor fila = db.rawQuery
                    ("select nombre, precio from articulos where codigo =" + codigo, null);
            //El metodo moveToFirst coloca en la primera posicion
            //fila.moveToFirst() se revisa si la consulta tiene valores para mostrar
            if (fila.moveToFirst()){
                //se le pasa el valor 0 porque es el primer valor que se colocara en un editText
                editTextNombre.setText(fila.getString(0));
                //El segundo valor es 1
                editTextPrecio.setText(fila.getString(1));
                //Se cierra la base de datos
                db.close();
            }else{
                //Mensaje para cuando el producto no existe
                Toast.makeText(this, "El producto no existe", Toast.LENGTH_LONG).show();
                db.close();
            }
        }else{
            Toast.makeText(this, "Debe introducir el codigo", Toast.LENGTH_LONG).show();
        }
    }

    public void Eliminar(View view){
        //Se crea una objeto de la clase con las operaciones de la DB (DBHelper)
        DBHelper admin = new DBHelper(this);
        //Abrir la base de dato en modo escritura y lectura
        SQLiteDatabase db = admin.getWritableDatabase();
        //Se crea la variable codigo para identificar cual es el producto que se quiere eliminar de la base de datos
        String codigo = editTextCodigo.getText().toString();
        //Se valida el campo
        if (!codigo.isEmpty()){
            //Se crea una variable de tipo entero por que el metodo delete devuelve un valor entero con los valores borrado
            //Se usa el metodo delete para borrar algun valor de la base de datos
            //Se pasa como parametro el nombre de la tabla, el codigo para saber donde se debe borrar
            int cantidad = db.delete(DBManager.TABLE_NAME, "codigo="+ codigo, null);
            //Cerrar la base de datos
            db.close();
            editTextCodigo.setText("");
            editTextNombre.setText("");
            editTextPrecio.setText("");
            //Cuando el metodo delete borre un articulo retornara un 1 caso contrario 0
            if (cantidad == 1) {
                Toast.makeText(this, "Articulo eliminado exitosamente", Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(this, "El articulo no existe", Toast.LENGTH_LONG).show();
            }
        }else{
            Toast.makeText(this, "Debe introducir el codigo", Toast.LENGTH_LONG).show();
        }
    }
    //Metodo para modificar un articulo o producto
    public void Modificar (View view){
        //Se crea una objeto de la clase con las operaciones de la DB (DBHelper)
        DBHelper admin = new DBHelper(this);
        //Abrir la base de dato en modo escritura y lectura
        SQLiteDatabase db = admin.getWritableDatabase();
        //Se crear las variables para recuperar el contendido de los editText
        //Esto se hace para que el usario pueda modificar cualquiera de los campos
        String codigo = editTextCodigo.getText().toString();
        String nombre = editTextNombre.getText().toString();
        String precio = editTextPrecio.getText().toString();
        //Se hacen validaciones para los campos
        if (!codigo.isEmpty() && !nombre.isEmpty() && !precio.isEmpty()){
            //Cuando se cumpla la condicion de que deben contener texto
            //Se crea un objeto de la clase ContentValues que se utiliza para almacenar un conjunto de datos
            //Y almacenara los datos nuevos
            ContentValues registro = new ContentValues();
            //Se colocan los datos nuevos para almacenarlo dentro de la base de datos
            registro.put("codigo", codigo);
            registro.put("nombre", nombre);
            registro.put("precio", precio);

            // A continuacion se deben guardar los nuevos datos en la base de datos
            //El metodo update devolvera la cantidad con los datos modificados
            //En los parametros se pasa el nombre de la tabla, el objeto que contiene las modificaciones
            //El elemento donde se modifcaran los productos
            int cantidad = db.update(DBManager.TABLE_NAME, registro, "codigo="+codigo, null);
            //Se cierra la base de datos despues de utilizarla
            db.close();


            if (cantidad == 1) {
                Toast.makeText(this, "Articulo modificado exitosamente", Toast.LENGTH_LONG).show();
                editTextCodigo.setText("");
                editTextNombre.setText("");
                editTextPrecio.setText("");
            }else{
                Toast.makeText(this, "El articulo no existe", Toast.LENGTH_LONG).show();
            }

        }else{
            //Se envia el toast con el mensaje
            Toast.makeText(this, "Debe llenar todos los campos", Toast.LENGTH_LONG).show();
        }
    }




}
